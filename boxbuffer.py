"""
Exports the BoxBuffer class.
"""

from cStringIO import StringIO

class BoxBuffer(object):

  """
  A StringIO-like buffer whose getvalue() method returns the buffer's contents wrapped in an ASCII box.

  The ASCII box is at least wide enough to accommodate the longest line in the buffer, unless max_width is specified.
  Includes the capability to insert horizontal lines within the buffer output (via the hline() instance method).
  User can specify custom horizontal and vertical border strings, horizontal line string, and top/right/bottom/left padding amounts.
  Maximum and minimum box widths can also be specified (measured from the outside of the box).
  """

  def __init__(self, hborder='=', vborder='||', hline='-', top_padding=1, right_padding=1, bottom_padding=1, left_padding=1, min_width=0, max_width=None):
    """
    Construct a new empty BoxBuffer using default border strings and paddings with no min/max width.

    Keyword arguments:
    hborder -- the string to repeat for the top and bottom borders (default: '=')
    vborder -- the string to use for the left and right borders (default: '||')
    hline -- the string to repeat for horizontal lines within the buffer (default: '-')
    top_padding -- the number of blank lines to insert between the top border and the first line of buffer contents (default: 1)
    right_padding -- the (minimum) number of spaces to insert between the end of each line of buffer contents and the right border (default: 1)
    bottom_padding -- the number of blank lines to insert between the last line of buffer contents and the bottom border (default: 1)
    left_padding -- the number of spaces to insert between the left border and the beginning of each line of buffer contents (default: 1)
    min_width -- the minimum width of the ASCII box enclosing the buffer contents (measured from the outside dimensions of the box)
    max_width -- the maximum width of the ASCII box enclosing the buffer contents (measured from the outside dimensions of the box)
                  if a line exceeds the length allowed by the max_width, it is split into multiple consecutive lines
    """
    self.lines = []
    self.closed = False
    self.__hborder = hborder
    self.__vborder = vborder
    self.__hline = hline
    self.__top_padding = max(0, top_padding)
    self.__right_padding = max(0, right_padding)
    self.__bottom_padding = max(0, bottom_padding)
    self.__left_padding = max(0, left_padding)
    self.__min_width = min_width
    self.__max_width = max_width
    if max_width is not None:
      if max_width < min_width:
        raise ValueError('max_width must be no less than min_width')
      if max_width <= 2 * len(self.__vborder) + self.__right_padding + self.__left_padding:
        raise ValueError('max_width must be greater than right and left paddings plus twice width of vertical border')

  def write(self, instr):
    """Append a string to the buffer's contents."""
    split_lines = instr.split('\n')
    if self.lines and isinstance(self.lines[-1], str): # append first line if last write left an un-terminated line
      self.lines[-1] += split_lines[0]
    else:
      self.lines.append(split_lines[0])
    for line in split_lines[1:]:
      self.lines.append(self.LineBreak())
      self.lines.append(line)

  def hline(self):
    """Append a horizontal line to the buffer's contents."""
    self.lines.append(self.HLine())

  def getvalue(self):
    """Return the string representing the buffer's contents wrapped in an ASCII box."""
    if self.closed:
      raise ValueError('attempting to read a closed buffer')
    text_width = max(map(len, (l for l in self.lines if isinstance(l, str))) + [self.__min_width - self.__left_padding - self.__right_padding - 2 * len(self.__vborder)])
    if self.__max_width is not None:
      text_width = min(text_width, self.__max_width - 2 * len(self.__vborder) - self.__right_padding - self.__left_padding)
    hwidth = max(map(len, (l for l in self.lines if isinstance(l, str))) + [self.__min_width]) + 2
    out_buf = StringIO()
    out_buf.write(self.__hborder * (text_width + self.__left_padding + self.__right_padding + 2 * len(self.__vborder)) + '\n') # top horizontal border
    for i in xrange(self.__top_padding):
      out_buf.write(self.__vborder + ' '*(self.__left_padding + text_width + self.__right_padding)+ self.__vborder + '\n') # top padding
    for l in self.lines:
      if isinstance(l, str):
        ltmp = l
        while ltmp:
          lpart = ltmp[:text_width]
          ltmp = ltmp[text_width:]
          out_buf.write('%s%s%s%s%s%s\n' % (self.__vborder, ' '*self.__left_padding, lpart, ' '*(text_width-len(lpart)), ' '*self.__right_padding, self.__vborder))
      elif isinstance(l, self.HLine):
        for i in xrange(self.__top_padding): # padding before hline
          out_buf.write(self.__vborder + ' '*(self.__left_padding + text_width + self.__right_padding) + self.__vborder + '\n')
        out_buf.write('%s%s%s\n' % (self.__vborder, self.__hline * ((text_width + self.__left_padding + self.__right_padding)/len(self.__hline)), self.__vborder)) # hline
        for i in xrange(self.__bottom_padding): # padding after hline
          out_buf.write(self.__vborder + ' '*(self.__left_padding + text_width + self.__right_padding) + self.__vborder + '\n')
    for i in xrange(self.__bottom_padding):
      out_buf.write(self.__vborder + ' '*(self.__left_padding + text_width + self.__right_padding)+ self.__vborder + '\n') # bottom padding
    out_buf.write(self.__hborder * (text_width + self.__left_padding + self.__right_padding + 2 * len(self.__vborder)) + '\n') # bottom horizontal border

    return out_buf.getvalue()

  def close(self):
    """Close this BoxBuffer.  The buffer's contents cannot be retrieved after calling close()."""
    self.closed = True
    self.buf.close()
    del self.buf

  class HLine:
    """Represents a horizontal line in the buffer."""
    pass

  class LineBreak:
    """Represents a line break in the buffer."""
    pass
